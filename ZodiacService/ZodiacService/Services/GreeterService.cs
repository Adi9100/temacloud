using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZodiacService
{
    public class GreeterService : Greeter.GreeterBase
    {
        private readonly ILogger<GreeterService> _logger;
        public GreeterService(ILogger<GreeterService> logger)
        {
            _logger = logger;
        }

        public override Task<HelloReply> SayHello(HelloRequest request, ServerCallContext context)
        {
           
            if (((request.Month == 3) && (request.Day >= 21 || request.Day <= 31)) || (request.Month == 4 && (request.Day >= 01 || request.Day <= 21)))
            {
                Console.WriteLine("Berbec ");
            }

            if (((request.Month == 4) && (request.Day >= 21 || request.Day <= 31)) || (request.Month == 5&& (request.Day >= 01 || request.Day <= 21)))
            {
                Console.WriteLine("Taur");
            }
            if (((request.Month == 5) && (request.Day >= 21 || request.Day <= 31)) || (request.Month == 6 && (request.Day >= 01 || request.Day <= 21)))
            {
                Console.WriteLine("Gemeni");
            }
            if (((request.Month == 6) && (request.Day >= 22 || request.Day <= 31)) || (request.Month == 7&& (request.Day >= 01 || request.Day <= 21)))
            {
                Console.WriteLine("Rac");
            }
            if (((request.Month == 7) && (request.Day >= 22 || request.Day <= 31)) || (request.Month == 8 && (request.Day >= 01 || request.Day <= 21)))
            {
                Console.WriteLine("Leu");
            }
            if (((request.Month == 8) && (request.Day >= 22 || request.Day <= 31)) || (request.Month == 9 && (request.Day >= 01 || request.Day <= 21)))
            {
                Console.WriteLine("Fecioara");
            }
            if (((request.Month == 9) && (request.Day >= 22|| request.Day <= 31)) || (request.Month == 10 && (request.Day >= 01 || request.Day <= 21)))
            {
                Console.WriteLine("Balanta");
            }
            if (((request.Month == 10) && (request.Day >= 22 || request.Day <= 31)) || (request.Month == 11 && (request.Day >= 01 || request.Day <= 21)))
            {
                Console.WriteLine("Scorpion");
            }
            if (((request.Month == 11) && (request.Day >= 22 || request.Day <= 31)) || (request.Month == 12 && (request.Day >= 01 || request.Day <= 21)))
            {
                Console.WriteLine("Sagetator");
            }
            if (((request.Month == 12) && (request.Day >= 22 || request.Day <= 31)) || (request.Month == 1 && (request.Day >= 01 || request.Day <= 21)))
            {
                Console.WriteLine("Capricorn");
            }
            if (((request.Month == 1) && (request.Day >= 22 || request.Day <= 31)) || (request.Month == 2 && (request.Day >= 01 || request.Day <= 21)))
            {
                Console.WriteLine("Varsator");
            }
            if (((request.Month == 2) && (request.Day >= 22 || request.Day <= 31)) || (request.Month == 3 && (request.Day >= 01 || request.Day <= 21)))
            {
                Console.WriteLine("Pesti");
            }
            return Task.FromResult(new HelloReply
            { 
                
            }) ;
        }
    }
}
