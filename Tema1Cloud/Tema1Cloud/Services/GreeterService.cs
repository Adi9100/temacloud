using Grpc.Core;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using System;

namespace Tema1Cloud
{
    public class GreeterService : Greeter.GreeterBase
    {
        private readonly ILogger<GreeterService> _logger;
        public GreeterService(ILogger<GreeterService> logger)
        {
            _logger = logger;
        }

        public override Task<HelloReply> SayHello(HelloRequest request, ServerCallContext context)
        {
            Console.WriteLine("Name: " + request.Name);
            Console.WriteLine("Cnp: " + request.Cnp);
            Console.WriteLine("Gender: " + request.Gender);
            Console.WriteLine("Age: " + request.Age);
            return Task.FromResult(new HelloReply
            {
                Message = "Succes"
            });
        }
    }
}