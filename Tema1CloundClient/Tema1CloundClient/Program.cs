﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Grpc.Net.Client;
using Tema1CloundClient;

namespace GrpcClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Greeter.GreeterClient(channel);

            Console.Write("Enter your name please: ");
            var name = Console.ReadLine();

            Console.Write("Enter your CNP please: ");
            var cnp = Console.ReadLine();

            var sex="";
            if (cnp[0] - '0' == 1 || cnp[0] - '0' == 5)
                sex= "Men";
            else
                if(cnp[0] - '0' == 2 ||cnp[0] - '0' == 6)
                   sex= "Women";
           else
            {
                Console.Write("Enter your CNP again please: ");
                cnp = Console.ReadLine();
            }
            string year, month, day;
            if (cnp[0] - '0' == 1 || cnp[0] - '0' == 2)
            {
                year = "19" + cnp[1] + cnp[2];
                month = cnp[3].ToString() + cnp[4].ToString();
                day = cnp[5].ToString() + cnp[6].ToString();
            }
            else
            {
                year = "20" + cnp[1] + cnp[2];
                month = cnp[3].ToString() + cnp[4].ToString();
                day = cnp[5].ToString() + cnp[6].ToString();
            }
             DateTime dateNow = DateTime.Now;
            DateTime dateCnp=new DateTime(Int32.Parse(year), Int32.Parse( month), Int32.Parse( day));
            double age=((dateNow - dateCnp).TotalDays)/365;
            var reply = await client.SayHelloAsync(new HelloRequest { Name = name, Cnp = cnp, Gender = sex, Age = (int)age });
            Console.WriteLine( reply.Message );
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}