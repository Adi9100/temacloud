﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Grpc.Net.Client;

namespace ZodiacClient
{

    class Program
    {
        static async Task Main(string[] args)
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            Console.WriteLine("Please enter your date of birth by format mm/dd/yyyy (e.g. 01/01/1999) : ");
            string date = Console.ReadLine();
            int dayToCheck=0 ;
            int monthToCheck=0 ;
            int yearToCheck=0 ;
            int onlynumbers = 1;
            for (int index = 0; index < date.Length; index++)
                if (!(Char.IsDigit(date[index])) && date[index] != '/')
                    onlynumbers = 0;
            while (onlynumbers == 0 || date.Length != 10 || date[2]!='/' || date[5]!='/')
            {
                Console.WriteLine("Eroare");
                date = Console.ReadLine();
                onlynumbers = 1;
                for (int index = 0; index < date.Length; index++)
                    if (!(Char.IsDigit(date[index])) && date[index] != '/')
                        onlynumbers = 0;
            }


            int inputverified = 0;
            while (inputverified != 1)
            {
                string day, month, year;
                if (date[0] == '0')
                    month = date.Substring(1, 1);
                else
                    month = date.Substring(0, 2);
                if (date[3] == '0')
                    day = date.Substring(4, 1);
                else
                    day = date.Substring(3, 2);
                year = date.Substring(6, 4);
                inputverified = 1;
                dayToCheck = Convert.ToInt32(day);
                monthToCheck = Convert.ToInt32(month);
                yearToCheck = Convert.ToInt32(year);

                int leapyear = 0;
                if (yearToCheck % 4 == 0 && yearToCheck % 100 != 0)
                    leapyear = 1;
                if (yearToCheck % 400 == 0)
                    leapyear = 1;

                if (yearToCheck > 2021 || yearToCheck < 1800)
                {
                    inputverified = 0;
                }
                if (monthToCheck < 1 || monthToCheck > 12)
                {
                    inputverified = 0;
                }
                if (dayToCheck < 1 || dayToCheck > 31)
                {
                    inputverified = 0;
                }

                if (monthToCheck == 4 || monthToCheck == 6 || monthToCheck == 9 || monthToCheck == 11)
                {
                    if (dayToCheck == 31)
                    {
                        inputverified = 0;
                    }
                }

                if (monthToCheck == 2)
                {
                    if (dayToCheck == 31 || dayToCheck == 30)
                    {
                        inputverified = 0;
                    }
                    if (leapyear == 0 && dayToCheck == 29)
                    {
                        inputverified = 0;
                    }
                }
                if (inputverified == 0)
                {

                    Console.WriteLine("Eroare");
                    date = Console.ReadLine();
                }

            }

             var client = new Greeter.GreeterClient(channel);
             var reply = await client.SayHelloAsync( new HelloRequest { Day=dayToCheck,Month=monthToCheck});
            Console.WriteLine( reply.Message);
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}